package com.abaenglish.authorization;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClientCredentialsTest {

    private static final String GRANT_TYPE = "client_credentials";
    private static final String CLIENT_ID = "credentialsId";
    private static final String CLIENT_SECRET = "credentiasSecret";
    private static final String EXPECTED_SCOPES = "read write trust";

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void getTokenViaClientCredentials() {
        given().auth().basic(CLIENT_ID, CLIENT_SECRET)
                .when()
                .post("/oauth/token?grant_type=" + GRANT_TYPE)
                .then()
                .assertThat()
                .statusCode(200)
                .body("access_token", notNullValue())
                .body("expires_in", notNullValue())
                .body("jti", notNullValue())
                .body("token_type", equalTo("bearer"))
                .body("scope", equalTo(EXPECTED_SCOPES))
                .body("userId", nullValue())
                .body("userType", nullValue());
    }

}
