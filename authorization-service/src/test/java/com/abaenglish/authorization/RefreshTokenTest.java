package com.abaenglish.authorization;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RefreshTokenTest {

    private static final String GRANT_TYPE = "refresh_token";
    private static final String CLIENT_ID = "passwordId";
    private static final String CLIENT_SECRET = "passwordSecret";
    private static final String EXPECTED_SCOPES = "read write";

    private static final String REFRESH_EXPIRED_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhc2R2YUBhYmFlbmdsaXNoLmNvbSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsInRydXN0Il0sImF0aSI6IjBjMTI0YzAyLTcxMDEtNGE1NS1iYjhiLTIzOTZmZDVmMWEwYSIsInVzZXJUeXBlIjoxLCJleHAiOjE0OTAxOTg4NTYsInVzZXJJZCI6IjkzNDEzMDIiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiYWZjZWJiODUtZDNlZi00ZWY2LWFhNjUtYzRiMTBlYjZjZGNhIiwiY2xpZW50X2lkIjoiYWJhd2ViYXBwcyJ9.M-mApQn8fRvwJxAO-5g2ojUJSjANRL7xgzIlZWdD8Zg";
    private static final String REFRESH_VALID_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhc2R2YUBhYmFlbmdsaXNoLmNvbSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJmM2UxN2QyNi04YmQyLTQwNTQtYjZmNS1mMTg1NzUxZWJmY2QiLCJleHAiOjM2MzkzODA0MTIsInVzZXJJZCI6IjczQzEzQkUyLTg4NEEtNEI3Mi1CMDdELUUyRDYzMTQxMUYxRiIsImF1dGhvcml0aWVzIjpbIlJPTEVfQkFTSUMiLCJST0xFX1VTRVIiXSwianRpIjoiYjQ0YjBkZmMtMWY5YS00ZTliLWE4YzgtYWRhNjJlMjI4MTJjIiwiY2xpZW50X2lkIjoicGFzc3dvcmRJZCJ9.uAIOmVXhC-Q4W2XUhF6jKn_7H7-jNW95E8CyfpbqFwM";


    private static final String USER_ID = "73C13BE2-884A-4B72-B07D-E2D631411F1F";
    private static final Integer USER_TYPE = 1;
    private static final String EMAIL = "asdva@abaenglish.com";

    @Value("${local.server.port}")
    int port;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        RestAssured.port = port;
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void refreshExpiredToken() {

        mockServer.expect(requestTo("https://api.test.aba.land/authorization/authorize"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"userId\": \"" + USER_ID + "\", \"userType\":\"" + USER_TYPE + "\", \"email\": \"" + EMAIL + "\"}", MediaType.APPLICATION_JSON));

        given().auth().basic(CLIENT_ID, CLIENT_SECRET)
                .when()
                .post("/oauth/token?grant_type=" + GRANT_TYPE + "&refresh_token=" + REFRESH_EXPIRED_TOKEN)
                .then()
                .assertThat()
                .statusCode(401)
                .body("access_token", nullValue())
                .body("refresh_token", nullValue())
                .body("expires_in", nullValue())
                .body("jti", nullValue())
        ;
    }

    @Test
    public void refreshValidToken() {

        mockServer.expect(requestTo("https://api.test.aba.land/authorization/authorize"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"userId\": \"" + USER_ID + "\", \"userType\":\"" + USER_TYPE + "\", \"email\": \"" + EMAIL + "\"}", MediaType.APPLICATION_JSON));

        given().auth().basic(CLIENT_ID, CLIENT_SECRET)
                .when()
                .post("/oauth/token?grant_type=" + GRANT_TYPE + "&refresh_token=" + REFRESH_VALID_TOKEN)
                .then()
                .assertThat()
                .statusCode(200)
                .body("access_token", notNullValue())
                .body("refresh_token", notNullValue())
                .body("expires_in", notNullValue())
                .body("jti", notNullValue())
                .body("token_type", equalTo("bearer"))
                .body("scope", equalTo(EXPECTED_SCOPES))
                .body("userId", equalTo(USER_ID))
        ;

    }

}
