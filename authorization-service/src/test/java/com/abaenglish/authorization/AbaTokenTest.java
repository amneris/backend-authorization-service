package com.abaenglish.authorization;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withUnauthorizedRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AbaTokenTest {

    private static final String GRANT_TYPE = "token";
    private static final String CLIENT_ID = "passwordId";
    private static final String CLIENT_SECRET = "passwordSecret";
    private static final String USER_ID = "73C13BE2-884A-4B72-B07D-E2D631411F1F";
    private static final Integer USER_TYPE = 1;
    private static final String USERNAME = "asdva@abaenglish.com";
    private static final String PASSWORD = "asdf";
    private static final String EXPECTED_SCOPES = "read write";

    @Value("${local.server.port}")
    int port;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        RestAssured.port = port;
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void testGetTokenViaUsernamePassword() {

        mockServer.expect(requestTo("https://api.test.aba.land/apiuser/login"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"email\": \"" + USERNAME + "\", \"userId\": \"" + USER_ID + "\", \"userType\":\"" + USER_TYPE + "\"}", MediaType.APPLICATION_JSON));

        given().auth().basic(CLIENT_ID, CLIENT_SECRET)
                .when()
                .post("/oauth/token?grant_type=" + GRANT_TYPE + "&username=" + USERNAME + "&password=" + PASSWORD)
                .then()
                .assertThat()
                .statusCode(200)
                .body("access_token", notNullValue())
                .body("refresh_token", notNullValue())
                .body("expires_in", notNullValue())
                .body("jti", notNullValue())
                .body("token_type", equalTo("bearer"))
                .body("scope", equalTo(EXPECTED_SCOPES))
                .body("userId", equalTo(USER_ID));
    }

    @Test
    public void testGetTokenViaWrongToken() {

        mockServer.expect(requestTo("https://api.test.aba.land/apiuser/login"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"email\": \"" + USERNAME + "wrong\", \"userId\": \"" + USER_ID + "\", \"userType\":\"" + USER_TYPE + "\"}", MediaType.APPLICATION_JSON));

        given().auth().basic(CLIENT_ID, CLIENT_SECRET)
                .when()
                .post("/oauth/token?grant_type=" + GRANT_TYPE + "&username=" + USERNAME + "&password=" + PASSWORD)
                .then()
                .log().all()
                .assertThat()
                .statusCode(401);
    }

    @Test
    public void testGetTokenViaInvalidToken() {

        mockServer.expect(requestTo("https://api.test.aba.land/apiuser/login"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withUnauthorizedRequest());

        given().auth().basic(CLIENT_ID, CLIENT_SECRET)
                .when()
                .post("/oauth/token?grant_type=" + GRANT_TYPE + "&username=" + USERNAME + "&password=" + PASSWORD)
                .then()
                .assertThat()
                .statusCode(401);
    }

}
