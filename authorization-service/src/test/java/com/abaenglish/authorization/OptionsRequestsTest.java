package com.abaenglish.authorization;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OptionsRequestsTest {

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void tokenEndpointTest() {
        given()
                .when()
                .options("/oauth/token")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void authorizeEndopointTest() {
        given()
                .when()
                .options("/oauth/authorize")
                .then()
                .assertThat()
                .statusCode(200);
    }

}
