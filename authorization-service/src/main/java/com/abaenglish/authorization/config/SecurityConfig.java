package com.abaenglish.authorization.config;

import com.abaenglish.authorization.provider.AbawebappsPasswordAuthenticationProvider;
import com.abaenglish.authorization.provider.AbawebappsTokenAuthenticationProvider;
import com.abaenglish.authorization.service.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AbawebappsPasswordAuthenticationProvider abawebappsPasswordAuthenticationProvider;
    private final AbawebappsTokenAuthenticationProvider abawebappsTokenAuthenticationProvider;
    private final CustomUserDetailsService customUserDetailsService;

    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(abawebappsTokenAuthenticationProvider)
                .authenticationProvider(abawebappsPasswordAuthenticationProvider)
                .userDetailsService(customUserDetailsService);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**");
    }

}
