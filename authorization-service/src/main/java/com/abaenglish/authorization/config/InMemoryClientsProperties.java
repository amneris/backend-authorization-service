package com.abaenglish.authorization.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@ConfigurationProperties(prefix = "security.oauth2.in-memory")
@Getter
@Setter
public class InMemoryClientsProperties {

    private List<AbaClientDetails> clients = new ArrayList<>();

    public static class AbaClientDetails extends BaseClientDetails {

        private static final long serialVersionUID = 5657083781143459462L;

        @org.codehaus.jackson.annotate.JsonProperty("authorities")
        @com.fasterxml.jackson.annotation.JsonProperty("authorities")
        public void setAuthorities(Set<String> values) {
            setAuthorities(AuthorityUtils.createAuthorityList(values
                    .toArray(new String[values.size()])));
        }

    }

}
