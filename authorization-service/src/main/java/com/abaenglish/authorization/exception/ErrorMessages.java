package com.abaenglish.authorization.exception;

import com.abaenglish.boot.exception.CodeMessage;

public enum ErrorMessages {

    LOGIN_WITH_PASSWORD_FAILED(new CodeMessage("BAS0001", "Login with password failed.")),
    LOGIN_WITH_TOKEN_FAILED(new CodeMessage("BAS0002", "Login with token failed."));

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
