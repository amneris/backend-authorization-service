package com.abaenglish.authorization.provider;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class AuthenticationUtils {

    private final RestTemplate restTemplate;
    private final AuthenticationProperties authenticationProperties;

    public AbaApiUser getUser(String loginPath, HttpHeaders headers) {
        ResponseEntity<AbaApiUser> response;
        try {

            response = restTemplate.exchange(
                    authenticationProperties.getAbaProvider().getHost() + loginPath,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    AbaApiUser.class);
        } catch (Exception e) {
            throw new InternalAuthenticationServiceException(e.getMessage(), e);
        }

        return response.getBody();
    }

    public Collection<GrantedAuthority> getAuthorities(int userType) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        authorities.add(new SimpleGrantedAuthority("ROLE_BASIC"));
        if (userType == 2) {
            authorities.add(new SimpleGrantedAuthority("ROLE_PREMIUM"));
        }

        return authorities;
    }

}
