package com.abaenglish.authorization.provider;

import com.abaenglish.authorization.config.AbawebappsTokenAuthenticationToken;
import com.abaenglish.authorization.exception.ErrorMessages;
import com.abaenglish.authorization.model.AuthenticatedUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Component
public class AbawebappsTokenAuthenticationProvider implements AuthenticationProvider {

    private static final String LOGIN_PATH = "/apiuser/login";

    private final AuthenticationUtils authenticationUtils;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        HttpHeaders headers = new HttpHeaders();
        headers.add("ABA_API_AUTH_TOKEN", password);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        AbaApiUser abaApiUser;
        try {
            abaApiUser = authenticationUtils.getUser(LOGIN_PATH, headers);
        } catch (Exception e) {
            log.error(ErrorMessages.LOGIN_WITH_TOKEN_FAILED.getError().getMessage(), e);
            throw new InternalAuthenticationServiceException(ErrorMessages.LOGIN_WITH_TOKEN_FAILED.getError().getMessage(), e);
        }

        Authentication loggedUser;
        if (username.equals(abaApiUser.getEmail())) {
            Collection<GrantedAuthority> authorities = authenticationUtils.getAuthorities(abaApiUser.getUserType());

            AuthenticatedUser authenticatedUser = AuthenticatedUser.builder()
                    .username(username)
                    .password(password)
                    .authorities(authorities)
                    .userId(abaApiUser.getUserId())
                    .build();

            loggedUser = new AbawebappsTokenAuthenticationToken(authenticatedUser, password, authorities);
        } else {
            throw new InternalAuthenticationServiceException(ErrorMessages.LOGIN_WITH_TOKEN_FAILED.getError().getMessage());
        }

        return loggedUser;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return AbawebappsTokenAuthenticationToken.class.isAssignableFrom(authentication);
    }

}
