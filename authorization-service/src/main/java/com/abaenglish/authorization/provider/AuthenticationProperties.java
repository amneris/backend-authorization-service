package com.abaenglish.authorization.provider;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "authentication")
@Getter
@Setter
public class AuthenticationProperties {

    private AbaProvider abaProvider;

    @Getter
    @Setter
    public static class AbaProvider {

        private String host;
        private String token;

    }
}
