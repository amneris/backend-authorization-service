package com.abaenglish.authorization.provider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AbaApiUser {

    private final String userId;
    private final Integer userType;
    private final String email;

}
