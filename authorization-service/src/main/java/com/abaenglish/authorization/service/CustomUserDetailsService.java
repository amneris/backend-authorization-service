package com.abaenglish.authorization.service;

import com.abaenglish.authorization.exception.ErrorMessages;
import com.abaenglish.authorization.model.AuthenticatedUser;
import com.abaenglish.authorization.provider.AbaApiUser;
import com.abaenglish.authorization.provider.AuthenticationProperties;
import com.abaenglish.authorization.provider.AuthenticationUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Component
public class CustomUserDetailsService implements UserDetailsService {

    private static final String LOGIN_PATH = "/authorization/authorize";

    private final AuthenticationProperties authenticationProperties;
    private final AuthenticationUtils authenticationUtils;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        HttpHeaders headers = new HttpHeaders();
        headers.add("X_TOKEN", authenticationProperties.getAbaProvider().getToken());
        headers.add("EMAIL", username);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        AbaApiUser abaApiUser = null;

        try {
            abaApiUser = authenticationUtils.getUser(LOGIN_PATH, headers);
        } catch (Exception e) {
            log.error(ErrorMessages.LOGIN_WITH_TOKEN_FAILED.getError().getMessage(), e);
            throw new InternalAuthenticationServiceException(ErrorMessages.LOGIN_WITH_TOKEN_FAILED.getError().getMessage(), e);
        }

        Collection<GrantedAuthority> authorities = authenticationUtils.getAuthorities(abaApiUser.getUserType());

        return AuthenticatedUser.builder()
                .username(username)
                .password("")
                .authorities(authorities)
                .userId(abaApiUser.getUserId())
                .build();
    }

}
