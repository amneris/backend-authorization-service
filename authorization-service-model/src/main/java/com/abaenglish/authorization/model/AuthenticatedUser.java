package com.abaenglish.authorization.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@EqualsAndHashCode(callSuper = true, exclude = { "userId" })
public class AuthenticatedUser extends User {

    private static final long serialVersionUID = -1491827521121373148L;

    private String userId;

    @Builder
    public AuthenticatedUser(String username, String password, Collection<? extends GrantedAuthority> authorities, String userId) {
        super(username, password, authorities);
        this.userId = userId;
    }

}
