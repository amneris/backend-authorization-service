# Authorization Service

https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2

## Get an access token

### Getting access token via GrantType implicit

```shell
curl -i -u asdva@abaenglish.com:asdf "http://localhost:8080/oauth/authorize?response_type=token&client_id=abawebapps&redirect_uri=http://localhost:8081/api/v1/users/265"
```

### Getting access token via GrantType password

```shell
$ curl -u abawebapps:vrmQUdx3Mxw8YrP4 http://localhost:8080/oauth/token -d "grant_type=password&username=asdva@abaenglish.com&password=asdf"
```

### Getting access token via Client Credentials

```shell
$ curl -u abawebapps:vrmQUdx3Mxw8YrP4 http://localhost:8080/oauth/token -d "grant_type=client_credentials"
```

### Getting access token via authorization code

* Open browser with URL ```http://localhost:8080/oauth/authorize?client_id=abawebapps&response_type=code&redirect_uri=http://localhost:8081/api/v1/users/265```
* Use username user, password testpass for login
* Copy the code parameter from the URL
* Execute ```curl -u abawebapps:vrmQUdx3Mxw8YrP4 http://localhost:8080/oauth/token -d "grant_type=authorization_code&redirect_uri=http://localhost:8081/api/v1/users/265&code=<code from URL>"```

NOTE: redirect_uri must be the same in both requests.