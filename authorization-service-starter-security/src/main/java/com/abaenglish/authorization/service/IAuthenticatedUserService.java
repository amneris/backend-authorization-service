package com.abaenglish.authorization.service;

public interface IAuthenticatedUserService {

    Long getUserId();

}
