package com.abaenglish.authorization.autoconfigure;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ResourceServerAutoConfiguration.class, GlobalMethodSecurityAutoConfiguration.class})
@TestPropertySource(properties = "security.oauth2.resource.jwt.key-value: keytest")
@EnableConfigurationProperties(ResourceServerProperties.class)
public class ApplicationTest {

    @Autowired
    ApplicationContext context;

    @Test
    public void resourceServerEnabled() {
        assertThat(this.context.getBean(ResourceServerAutoConfiguration.class)).isNotNull();
        assertThat(this.context.getBean(GlobalMethodSecurityAutoConfiguration.class)).isNotNull();
    }

}
