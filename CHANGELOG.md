# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## UNRELEASED
### Add
- Implement oauth2 + jwt
- Use of properties as in memory clients information
- Call new authorization entpoint with X_TOKEN & EMAIL
- Refresh Token Test
- Get authorities by userType
- Change aba-boot-starter-parent version to 1.6.0 
